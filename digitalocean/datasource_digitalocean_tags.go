package digitalocean

import (
	"github.com/digitalocean/godo"
)

func flattenDigitalOceanTag(tag, meta interface{}, extra map[string]interface{}) (map[string]interface{}, error) {
	t := tag.(godo.Tag)

	flattenedTag := map[string]interface{}{}
	flattenedTag["name"] = t.Name
	flattenedTag["total_resource_count"] = t.Resources.Count
	flattenedTag["droplets_count"] = t.Resources.Droplets.Count
	flattenedTag["images_count"] = t.Resources.Images.Count
	flattenedTag["volumes_count"] = t.Resources.Volumes.Count
	flattenedTag["volume_snapshots_count"] = t.Resources.VolumeSnapshots.Count
	flattenedTag["databases_count"] = t.Resources.Databases.Count

	return flattenedTag, nil
}
